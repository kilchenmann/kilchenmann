## Hi there I'm André

Web developer from Switzerland. I got my PhD in media studies / digital humanitis from the University of Basel. Today I work as an archive computer scientist in Solothurn. I like good and functional design, create websites, and in my spare time I build my own furnitures, sew clothes, accessories and take photographs. The products are published on one of my [websites][website].

## Connect with me

### Social Media

[<img src="https://img.shields.io/badge/linkedin--blue?style=for-the-badge&logo=linkedin" alt="LinkedIn | Kilchenmann" />][linkedin]
[<img src="https://img.shields.io/badge/unsplash--darkgrey?style=for-the-badge&logo=unsplash" alt="Unsplash | Kilchenmann" />][unsplash]
[<img src="https://img.shields.io/badge/instagram--e1306c?style=for-the-badge&logo=instagram" alt="Instagram | Milchkannen" />][instagram]
[<img src="https://img.shields.io/badge/twitter--00acee?style=for-the-badge&logo=twitter" alt="Twitter | Milchkannen" />][twitter]
[<img src="https://img.shields.io/badge/pinterest--e60023?style=for-the-badge&logo=pinterest" alt="Pinterest | Milchkannen" />][pinterest]

### Social Coding

[<img src="https://img.shields.io/badge/github--purple?style=for-the-badge&logo=github" alt="GitHub | Kilchenmann" />][github]
[<img src="https://img.shields.io/badge/gitlab--orange?style=for-the-badge&logo=gitlab" alt="GitLab | Kilchenmann" />][gitlab]
[<img src="https://img.shields.io/badge/codepen--green?style=for-the-badge&logo=codepen" alt="CodePen | Kilchenmann" />][codepen]


## Languages and Frameworks I work with

### Frameworks

[<img src="https://img.shields.io/badge/angular--a6120d?style=for-the-badge&logo=angular" alt="Angular" />][angular]
[<img src="https://img.shields.io/badge/svelte--ff3e00?style=for-the-badge&logo=svelte" alt="Svelte" />][svelte]

### Coding

[<img src="https://img.shields.io/badge/typescript--1493ff?style=for-the-badge&logo=typescript" alt="TypeScript" />][typescript]
[<img src="https://img.shields.io/badge/bash--brightgreen?style=for-the-badge&logo=gnubash" alt="GNU Bash" />][gnubash]
[<img src="https://img.shields.io/badge/elm--blue?style=for-the-badge&logo=elm" alt="Elm" />][elm]
[<img src="https://img.shields.io/badge/html5--e34c26?style=for-the-badge&logo=html5" alt="Html 5" />][html5]
[<img src="https://img.shields.io/badge/sass--bf4080?style=for-the-badge&logo=sass" alt="SASS" />][sass]



### Writing

[<img src="https://img.shields.io/badge/latex--008080?style=for-the-badge&logo=latex" alt="LaTeX" />][latex]
[<img src="https://img.shields.io/badge/markdown--blue?style=for-the-badge&logo=markdown" alt="Markdown" />][markdown]


## Tools and Software I use most

### Blogs and Documentation

[<img src="https://img.shields.io/badge/ghost--30cf43?style=for-the-badge&logo=ghost" alt="Ghost" />][ghost]
[<img src="https://img.shields.io/badge/eleventy--black?style=for-the-badge&logo=eleventy" alt="11ty" />][11ty]
[<img src="https://img.shields.io/badge/mkdocs--blue?style=for-the-badge&logo=mkdocs" alt="mkDocs" />][mkdocs]

### Development and Deployment

[<img src="https://img.shields.io/badge/vsc--0078d7?style=for-the-badge&logo=visualstudiocode" alt="Visual Studio Code" />][vsc]
[<img src="https://img.shields.io/badge/docker--blue?style=for-the-badge&logo=docker" alt="Docker" />][docker]

### Notes and Design

[<img src="https://img.shields.io/badge/zettlr--33A67C?style=for-the-badge&logo=zettlr" alt="Zettlr" />][zettlr]
[<img src="https://img.shields.io/badge/figma--ff3b00?style=for-the-badge&logo=figma" alt="Figma" />][figma]



[website]: https://lakto.design
[twitter]: https://twitter.com/milchkannen
[instagram]: https://instagram.com/milchkannen
[github]: https://github.com/kilchenmann
[gitlab]: https://gitlab.com/kilchenmann
[linkedin]: https://linkedin.com/in/kilchenmann
[unsplash]: https://unsplash.com/@kilchenmann
[pinterest]: https://www.pinterest.ch/milchkannen
[codepen]: https://codepen.io/kilchenmann

[angular]: https://angular.io
[svelte]: https://svelte.dev
[typescript]: https://www.typescriptlang.org
[html5]: https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5
[sass]: https://sass-lang.com
[ghost]: https://ghost.org
[11ty]: https://www.11ty.dev
[docker]: https://www.docker.com
[mkdocs]: https://www.mkdocs.org
[elm]: https://elm-lang.org
[figma]: https://www.figma.com 
[vsc]: https://code.visualstudio.com
[zettlr]: https://www.zettlr.com
[latex]: https://www.latex-project.org
[gnubash]: https://www.gnu.org/software/bash
[markdown]: https://www.markdownguide.org/
